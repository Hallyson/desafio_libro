# Desafio Libro

A Libro Studio acaba de lançar uma nova plataforma de ensino online, onde podemos realizar a matrícula de alunos em cursos, através de um painel administrativo.


## Instalação

Faça o clone deste repositório

```bash
git clone https://Hallyson@bitbucket.org/Hallyson/desafio_libro.git
```

Instale as dependencias do COMPOSER
```bash
composer install
```

crie o arquivo .ENV
```bash
cp .env.example .env
php artisan key:generate
```

Assumindo que o banco está instalado, rodando e os dados do arquivo .ENV estão OK.
Rode as migrações e instale o passport
```bash
php artisan migrate
php artisan passport:install
```

Limpar os caches da instalação anterior

```bash
php artisan cache:clear
php artisan route:clear
php artisan view:clear
php artisan config:cache
```

Executar o servidor local
```bash
php artisan serve
```

## Licença
[MIT](https://choosealicense.com/licenses/mit/)
 