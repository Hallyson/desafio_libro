<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\EnrollmentRequest;
use App\Models\Enrollment;
use Illuminate\Http\Request;

class EnrollmentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $enrollments = (new Enrollment())->getEnrollments();
        return $this->sendResponse($enrollments, "Matrículas exibidas com sucesso.");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EnrollmentRequest $request)
    {
        $input = $request->validated();
        $create = Enrollment::create($input);

        $enrollment = (new Enrollment())->getEnrollments($create['id']);

        return $this->sendResponse($enrollment, "Matrícula realizada com sucesso.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $enrollment = (new Enrollment())->getEnrollments($id);

        if (is_null($enrollment)) {
            return $this->sendError('Matrícula não encontrado.');
        }

        return $this->sendResponse($enrollment, 'Matrícula encontrado.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EnrollmentRequest $request, Enrollment $enrollment)
    {
        $input  = $request->validated();

        $enrollment->student_id = $input['student_id'];
        $enrollment->course_id  = $input['course_id'];
        $enrollment->save();

        return $this->sendResponse($enrollment, 'Matrícula atualizada com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enrollment $enrollment)
    {
        $enrollment->delete();
        return $this->sendResponse([], 'Matrícula excluída com sucesso.');
    }

    public function count()
    {
        $count = (new Enrollment())->countStudentsByCourses();
        return $this->sendResponse($count, "Contagem exibida com sucesso");
    }
}
