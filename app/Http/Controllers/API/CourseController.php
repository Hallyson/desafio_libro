<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\CourseRequest;
use App\Models\Course;
use App\Http\Resources\Course as CourseResource;

class CourseController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $courses = Course::all();
        return $this->sendResponse(CourseResource::collection($courses), 'Cursos exibidos com sucesso');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CourseRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CourseRequest $request)
    {
        $input  = $request->validated();
        $course = Course::create($input);

        return $this->sendResponse(new CourseResource($course), 'Curso criado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $course = Course::find($id);

        if (is_null($course)) {
            return $this->sendError('Curso não encontrado.');
        }

        return $this->sendResponse(new CourseResource($course), 'Curso encontrado.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CourseRequest  $request
     * @param  Course  $course
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CourseRequest $request, Course $course)
    {
        $input  = $request->validated();

        $course->title = $input['title'];
        $course->description = $input['description'];
        $course->save();

        return $this->sendResponse(new CourseResource($course), 'Curso atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return $this->sendResponse([], 'Curso excluído com sucesso.');
    }
}
