<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\StudentRequest;
use App\Http\Resources\Student as StudentResource;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        return $this->sendResponse(StudentResource::collection($students), 'Alunos exibidos com sucesso');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StudentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        $input = $request->validated();
        $student = Student::create($input);

        return $this->sendResponse(new StudentResource($student), 'Aluno cadastrado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);

        if (is_null($student)) {
            return $this->sendError('Curso não encontrado.');
        }

        return $this->sendResponse(new StudentResource($student), 'Aluno encontrado.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\StudentRequest  $request
     * @param  Student  $student
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StudentRequest $request, Student $student)
    {
        $input  = $request->validated();

        $student->name       = $input['name'];
        $student->email      = $input['email'];
        $student->gender     = $input['gender'];
        $student->birth_date = $input['birth_date'];
        $student->save();

        return $this->sendResponse(new StudentResource($student), 'Aluno atualizado com sucesso.');
    }

    public function searchStudent(Request $request)
    {
        $result = (new Student())->search($request);

        return $this->sendResponse($result, "Aluno(s) encontrado(s) com sucesso");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Student  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $course)
    {
        $course->delete();
        return $this->sendResponse([], 'Aluno excluído com sucesso.');
    }
}
