<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'email', 'gender', 'birth_date'];

    protected $dates = ['birth_date'];

    public function search($request)
    {
        $query = DB::table('students')->select('name', 'email', 'gender', 'birth_date');

        if ($request->route('type') == 'name') {
            $query->where('name', 'LIKE',  "%{$request->route('value')}%");
        } else {
            $query->where('email', 'LIKE', "%{$request->route('value')}%");
        }

        return $query->get();
    }
}
