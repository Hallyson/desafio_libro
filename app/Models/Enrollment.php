<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Enrollment extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['course_id', 'student_id'];

    public function enrollments()
    {
        return $this->belongsToMany(Enrollment::class, 'enrollments', 'course_id', 'student_id');
    }

    public function getEnrollments($enrollment_id = null, $course_id = null, $student_id = null)
    {
        $query = DB::table('enrollments')->select(
            'enrollments.id',
            'students.name',
            'students.email',
            DB::raw("YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(students.birth_date))) AS age"),
            'students.gender',
            'courses.title',
            'courses.description'
        )
            ->join('students', 'students.id', '=', 'enrollments.student_id')
            ->join('courses', 'courses.id', '=', 'enrollments.course_id');

        if (!empty($course_id))
            $query->where('courses.id', $course_id);

        if (!empty($student_id))
            $query->where('students.id', $student_id);

        if (!empty($enrollment_id))
            $query->where('enrollments.id', $enrollment_id);


        return $query->get();
    }

    public function countStudentsByCourses()
    {
        return DB::table('students')
            ->select(
                'courses.title',
                DB::raw("CASE
             WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(students.birth_date))) < 15 THEN 'menor que 15 anos'
             WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(students.birth_date))) BETWEEN 15 AND 18 THEN 'entre 15 e 18 anos'
             WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(students.birth_date))) BETWEEN 18 AND 24 THEN 'entre 19 e 24 anos'
             WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(students.birth_date))) BETWEEN 25 AND 30 THEN 'entre 25 e 30 anos'
             WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(students.birth_date))) > 30 THEN 'maior que 30 anos'
             END AS faixa_etaria"),
                DB::raw("CASE
             WHEN students.gender = 'M' THEN 'Masculino'
             WHEN students.gender = 'F' THEN 'Feminino'
             END AS 'genero'"),
                DB::raw("count(*) as quantity")
            )
            ->join('enrollments', 'enrollments.student_id', '=', 'students.id')
            ->join('courses', 'enrollments.course_id', '=', 'courses.id')
            ->groupBy('courses.id', 'students.id')->get();
    }
}
