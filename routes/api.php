<?php

use App\Http\Controllers\API\CourseController;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\StudentController;
use App\Http\Controllers\API\EnrollmentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);

Route::middleware('auth:api')->group(function () {
    Route::resource('courses', CourseController::class);
    Route::resource('students', StudentController::class);
    Route::resource('enrollments', EnrollmentController::class);
    Route::get('students/{type}/{value}', [StudentController::class, 'searchStudent']);
    Route::get('count', [EnrollmentController::class, 'count']);
});
